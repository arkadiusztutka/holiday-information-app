FROM openjdk:11.0.2-jdk-stretch

LABEL maintainer="raider470@gmail.com"

VOLUME /tmp

EXPOSE 8080

ARG JAR_FILE=target/holidayapp-0.0.1-SNAPSHOT.jar

ADD ${JAR_FILE} holidayapp.jar

ENTRYPOINT ["java","-Djava.security.egd=file:/dev/./urandom","-jar","/holidayapp.jar"]