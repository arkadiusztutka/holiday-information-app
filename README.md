# Holidayapp

Application has a GET method(path /getNextSameDayHoliday) to getting information about closest holiday that occurs the same day in two countries.

# Technologies

- Java 11
- Spring Boot 2.1.3
- REST
- Mockito

# Parameters

| Name | Required | Description |
| --- | --- | --- |
| date | yes | Date in format YYYY-MM-DD
| countryCode1 | yes | Country code in format ISO 3166-1 alpha-2
| countryCode2 | yes | Country code in format ISO 3166-1 alpha-2
| maxYears | no | How many years should be searched to find holiday. Without setting this value application using default value from configuration 


# Example request and response


Request:
```
/nextSameDayHoliday?date=2018-12-30&countryCode1=GB&countryCode2=PL
```

Response:

```
{
    "date": "2019-01-01",
    "holidayCountry1": "New Year's Day",
    "holidayCountry2": "Nowy Rok"
}
```


# Additional information

File application.properties contains configuration information e.g. timeout. 
I assume that application searching for holidays have specific defined range of years. 
By default years to search holiday is set for 10 years.
It can be changed by the user himself using the additional parameter in the 'maxYears' request, it can determine how many years to search for.
The value 0 means searching only in the current year.


# Run

```
mvn spring-boot:run
```

