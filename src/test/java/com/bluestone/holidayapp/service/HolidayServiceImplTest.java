package com.bluestone.holidayapp.service;

import java.time.LocalDate;
import java.util.List;
import java.util.concurrent.CompletableFuture;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.TimeoutException;

import com.bluestone.holidayapp.exception.CountryNotSupportedException;
import com.bluestone.holidayapp.exception.NextSameDayHolidayNotFoundException;
import com.bluestone.holidayapp.request.HolidayRequest;
import com.bluestone.holidayapp.request.NextSameDayHolidayRequest;
import com.bluestone.holidayapp.response.NextSameDayHolidayResponse;
import com.bluestone.holidayapp.service.external.Holiday;
import com.bluestone.holidayapp.service.external.HolidayExternalService;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.ArgumentCaptor;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;
import org.springframework.test.util.ReflectionTestUtils;

import static org.assertj.core.api.Assertions.assertThat;
import static org.assertj.core.api.Assertions.assertThatThrownBy;
import static org.mockito.ArgumentMatchers.notNull;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

@RunWith(MockitoJUnitRunner.class)
public class HolidayServiceImplTest
{
    @Mock
    private HolidayExternalService holidayExternalService;

    @InjectMocks
    private HolidayServiceImpl service;

    @Before
    public void setUp()
    {
        ReflectionTestUtils.setField(service, "timeout", 1);
        ReflectionTestUtils.setField(service, "defaultMaxYears", 10);
    }

    @Test
    public void testGetNextSameDayHolidayWithoutMaxYears() throws ExecutionException, InterruptedException
    {
        var date = LocalDate.of(2019, 4, 3);
        var countryCode1 = "PL";
        var countryCode2 = "GB";
        var request = mock(NextSameDayHolidayRequest.class);
        var holidayRequestCaptor = ArgumentCaptor.forClass(HolidayRequest.class);

        var localDate1 = LocalDate.of(2019, 1, 1);
        var country1 = "PL";
        var name1 = "name1";
        var holiday1 = createHolidayMock(localDate1, country1, name1);
        var localDate2 = LocalDate.of(2019, 4, 28);
        var country2 = "PL";
        var name2 = "name2";
        var holiday2 = createHolidayMock(localDate2, country2, name2);
        var localDate3 = LocalDate.of(2019, 5, 1);
        var country3 = "PL";
        var name3 = "name3";
        var holiday3 = createHolidayMock(localDate3, country3, name3);
        var localDate4 = LocalDate.of(2019, 6, 1);
        var country4 = "PL";
        var name4 = "name4";
        var holiday4 = createHolidayMock(localDate4, country4, name4);
        var localDate5 = LocalDate.of(2019, 1, 1);
        var country5 = "GB";
        var name5 = "name5";
        var holiday5 = createHolidayMock(localDate5, country5, name5);
        var localDate6 = LocalDate.of(2019, 5, 1);
        var country6 = "GB";
        var name6 = "name6";
        var holiday6 = createHolidayMock(localDate6, country6, name6);
        var localDate7 = LocalDate.of(2019, 5, 11);
        var country7 = "GB";
        var name7 = "name7";
        var holiday7 = createHolidayMock(localDate7, country7, name7);

        when(request.getMaxYears()).thenReturn(null);
        when(request.getDate()).thenReturn(date);
        when(request.getCountryCode1()).thenReturn(countryCode1);
        when(request.getCountryCode2()).thenReturn(countryCode2);

        var completableFuture1 = CompletableFuture.completedFuture(List.of(holiday1, holiday2, holiday3, holiday4));
        var completableFuture2 = CompletableFuture.completedFuture(List.of(holiday5, holiday6, holiday7));
        when(holidayExternalService.searchHolidays(notNull())).thenReturn(completableFuture1, completableFuture2);

        NextSameDayHolidayResponse result = service.getNextSameDayHoliday(request);

        verify(request).getMaxYears();
        verify(request).getDate();
        verify(request).getCountryCode1();
        verify(request).getCountryCode2();
        verify(holidayExternalService, times(2)).searchHolidays(holidayRequestCaptor.capture());

        assertThat(result).isNotNull();
        assertThat(result.getDate()).isEqualTo(localDate3);
        assertThat(result.getName1()).isEqualTo(name3);
        assertThat(result.getName2()).isEqualTo(name6);

        List<HolidayRequest> holidayRequests = holidayRequestCaptor.getAllValues();
        assertThat(holidayRequests).hasSize(2);
        assertThat(holidayRequests.get(0).getCountryCode()).isEqualTo(countryCode1);
        assertThat(holidayRequests.get(0).getYear()).isEqualTo(date.getYear());
        assertThat(holidayRequests.get(1).getCountryCode()).isEqualTo(countryCode2);
        assertThat(holidayRequests.get(1).getYear()).isEqualTo(date.getYear());
    }

    @Test
    public void testGetNextSameDayHolidayWithMaxYears() throws ExecutionException, InterruptedException
    {
        var date = LocalDate.of(2019, 4, 3);
        var countryCode1 = "PL";
        var countryCode2 = "GB";
        var maxYears = 1;
        var request = mock(NextSameDayHolidayRequest.class);
        var holidayRequestCaptor = ArgumentCaptor.forClass(HolidayRequest.class);

        var localDate1 = LocalDate.of(2019, 1, 1);
        var country1 = "PL";
        var name1 = "name1";
        var holiday1 = createHolidayMock(localDate1, country1, name1);
        var localDate2 = LocalDate.of(2019, 4, 28);
        var country2 = "PL";
        var name2 = "name2";
        var holiday2 = createHolidayMock(localDate2, country2, name2);
        var localDate3 = LocalDate.of(2020, 5, 1);
        var country3 = "PL";
        var name3 = "name3";
        var holiday3 = createHolidayMock(localDate3, country3, name3);
        var localDate4 = LocalDate.of(2020, 6, 1);
        var country4 = "PL";
        var name4 = "name4";
        var holiday4 = createHolidayMock(localDate4, country4, name4);
        var localDate5 = LocalDate.of(2019, 1, 1);
        var country5 = "GB";
        var name5 = "name5";
        var holiday5 = createHolidayMock(localDate5, country5, name5);
        var localDate6 = LocalDate.of(2020, 5, 2);
        var country6 = "GB";
        var name6 = "name6";
        var holiday6 = createHolidayMock(localDate6, country6, name6);
        var localDate7 = LocalDate.of(2020, 6, 1);
        var country7 = "GB";
        var name7 = "name7";
        var holiday7 = createHolidayMock(localDate7, country7, name7);

        when(request.getMaxYears()).thenReturn(maxYears);
        when(request.getDate()).thenReturn(date);
        when(request.getCountryCode1()).thenReturn(countryCode1);
        when(request.getCountryCode2()).thenReturn(countryCode2);

        var completableFuture1 = CompletableFuture.completedFuture(List.of(holiday1, holiday2));
        var completableFuture2 = CompletableFuture.completedFuture(List.of(holiday5));
        var completableFuture3 = CompletableFuture.completedFuture(List.of(holiday3, holiday4));
        var completableFuture4 = CompletableFuture.completedFuture(List.of(holiday6, holiday7));
        when(holidayExternalService.searchHolidays(notNull())).thenReturn(completableFuture1, completableFuture2,
                completableFuture3, completableFuture4);

        NextSameDayHolidayResponse result = service.getNextSameDayHoliday(request);

        verify(request, times(2)).getMaxYears();
        verify(request).getDate();
        verify(request, times(2)).getCountryCode1();
        verify(request, times(2)).getCountryCode2();
        verify(holidayExternalService, times(4)).searchHolidays(holidayRequestCaptor.capture());

        assertThat(result).isNotNull();
        assertThat(result.getDate()).isEqualTo(localDate4);
        assertThat(result.getName1()).isEqualTo(name4);
        assertThat(result.getName2()).isEqualTo(name7);

        List<HolidayRequest> holidayRequests = holidayRequestCaptor.getAllValues();
        assertThat(holidayRequests).hasSize(4);
        assertThat(holidayRequests.get(0).getCountryCode()).isEqualTo(countryCode1);
        assertThat(holidayRequests.get(0).getYear()).isEqualTo(date.getYear());
        assertThat(holidayRequests.get(1).getCountryCode()).isEqualTo(countryCode2);
        assertThat(holidayRequests.get(1).getYear()).isEqualTo(date.getYear());
        assertThat(holidayRequests.get(2).getCountryCode()).isEqualTo(countryCode1);
        assertThat(holidayRequests.get(2).getYear()).isEqualTo(date.getYear()+1);
        assertThat(holidayRequests.get(3).getCountryCode()).isEqualTo(countryCode2);
        assertThat(holidayRequests.get(3).getYear()).isEqualTo(date.getYear()+1);
    }

    @Test
    public void testGetNextSameDayHolidayNotFound() throws ExecutionException, InterruptedException
    {
        var date = LocalDate.of(2019, 4, 3);
        var countryCode1 = "PL";
        var countryCode2 = "GB";
        var maxYears = 0;
        var request = mock(NextSameDayHolidayRequest.class);
        var holidayRequestCaptor = ArgumentCaptor.forClass(HolidayRequest.class);

        var localDate1 = LocalDate.of(2019, 1, 1);
        var country1 = "PL";
        var name1 = "name1";
        var holiday1 = createHolidayMock(localDate1, country1, name1);
        var localDate2 = LocalDate.of(2019, 4, 28);
        var country2 = "PL";
        var name2 = "name2";
        var holiday2 = createHolidayMock(localDate2, country2, name2);
        var localDate3 = LocalDate.of(2019, 1, 1);
        var country3 = "GB";
        var name3 = "name3";
        var holiday3 = createHolidayMock(localDate3, country3, name3);

        when(request.getMaxYears()).thenReturn(maxYears);
        when(request.getDate()).thenReturn(date);
        when(request.getCountryCode1()).thenReturn(countryCode1);
        when(request.getCountryCode2()).thenReturn(countryCode2);

        var completableFuture1 = CompletableFuture.completedFuture(List.of(holiday1, holiday2));
        var completableFuture2 = CompletableFuture.completedFuture(List.of(holiday3));
        when(holidayExternalService.searchHolidays(notNull())).thenReturn(completableFuture1, completableFuture2);

        assertThatThrownBy(() -> service.getNextSameDayHoliday(request)).isInstanceOf(NextSameDayHolidayNotFoundException.class);

        verify(request, times(2)).getMaxYears();
        verify(request).getDate();
        verify(request, times(2)).getCountryCode1();
        verify(request, times(2)).getCountryCode2();
        verify(holidayExternalService, times(2)).searchHolidays(holidayRequestCaptor.capture());

        List<HolidayRequest> holidayRequests = holidayRequestCaptor.getAllValues();
        assertThat(holidayRequests).hasSize(2);
        assertThat(holidayRequests.get(0).getCountryCode()).isEqualTo(countryCode1);
        assertThat(holidayRequests.get(0).getYear()).isEqualTo(date.getYear());
        assertThat(holidayRequests.get(1).getCountryCode()).isEqualTo(countryCode2);
        assertThat(holidayRequests.get(1).getYear()).isEqualTo(date.getYear());
    }

    @Test
    public void testGetNextSameDayHolidayWithTimeoutException() throws ExecutionException, InterruptedException
    {
        var date = LocalDate.of(2019, 4, 3);
        var countryCode1 = "PL";
        var countryCode2 = "GB";
        var request = mock(NextSameDayHolidayRequest.class);
        var holidayRequestCaptor = ArgumentCaptor.forClass(HolidayRequest.class);

        when(request.getMaxYears()).thenReturn(null);
        when(request.getDate()).thenReturn(date);
        when(request.getCountryCode1()).thenReturn(countryCode1);
        when(request.getCountryCode2()).thenReturn(countryCode2);

        var completableFuture1 = mock(CompletableFuture.class);
        var completableFuture2 = mock(CompletableFuture.class);
        when(holidayExternalService.searchHolidays(notNull())).thenReturn(completableFuture1, completableFuture2);

        assertThatThrownBy(() -> service.getNextSameDayHoliday(request))
                .isInstanceOf(ExecutionException.class)
                .hasCauseInstanceOf(TimeoutException.class);

        verify(request).getMaxYears();
        verify(request).getDate();
        verify(request).getCountryCode1();
        verify(request).getCountryCode2();
        verify(holidayExternalService, times(2)).searchHolidays(holidayRequestCaptor.capture());

        List<HolidayRequest> holidayRequests = holidayRequestCaptor.getAllValues();
        assertThat(holidayRequests).hasSize(2);
        assertThat(holidayRequests.get(0).getCountryCode()).isEqualTo(countryCode1);
        assertThat(holidayRequests.get(0).getYear()).isEqualTo(date.getYear());
        assertThat(holidayRequests.get(1).getCountryCode()).isEqualTo(countryCode2);
        assertThat(holidayRequests.get(1).getYear()).isEqualTo(date.getYear());
    }

    @Test
    public void testGetNextSameDayHolidayWithCountryNotSupportedException() throws ExecutionException, InterruptedException
    {
        var date = LocalDate.of(2019, 4, 3);
        var countryCode1 = "PL";
        var countryCode2 = "GB";
        var request = mock(NextSameDayHolidayRequest.class);
        var countryNotSupportedException = new CountryNotSupportedException(countryCode1);
        var holidayRequestCaptor = ArgumentCaptor.forClass(HolidayRequest.class);

        var localDate1 = LocalDate.of(2019, 1, 1);
        var country1 = "GB";
        var name1 = "name1";
        var holiday1 = createHolidayMock(localDate1, country1, name1);

        when(request.getMaxYears()).thenReturn(null);
        when(request.getDate()).thenReturn(date);
        when(request.getCountryCode1()).thenReturn(countryCode1);
        when(request.getCountryCode2()).thenReturn(countryCode2);

        CompletableFuture<List<Holiday>> completableFuture1 = CompletableFuture.failedFuture(countryNotSupportedException);
        CompletableFuture<List<Holiday>> completableFuture2 = CompletableFuture.completedFuture(List.of(holiday1));
        when(holidayExternalService.searchHolidays(notNull())).thenReturn(completableFuture1, completableFuture2);

        assertThatThrownBy(() -> service.getNextSameDayHoliday(request))
                .isInstanceOf(ExecutionException.class)
                .hasCause(countryNotSupportedException);

        verify(request).getMaxYears();
        verify(request).getDate();
        verify(request).getCountryCode1();
        verify(request).getCountryCode2();
        verify(holidayExternalService, times(2)).searchHolidays(holidayRequestCaptor.capture());

        List<HolidayRequest> holidayRequests = holidayRequestCaptor.getAllValues();
        assertThat(holidayRequests).hasSize(2);
        assertThat(holidayRequests.get(0).getCountryCode()).isEqualTo(countryCode1);
        assertThat(holidayRequests.get(0).getYear()).isEqualTo(date.getYear());
        assertThat(holidayRequests.get(1).getCountryCode()).isEqualTo(countryCode2);
        assertThat(holidayRequests.get(1).getYear()).isEqualTo(date.getYear());
    }

    private static Holiday createHolidayMock(LocalDate localDate, String country, String name)
    {
        var holiday = mock(Holiday.class);
        when(holiday.getDate()).thenReturn(localDate);
        when(holiday.getCountry()).thenReturn(country);
        when(holiday.getName()).thenReturn(name);
        return holiday;
    }


}