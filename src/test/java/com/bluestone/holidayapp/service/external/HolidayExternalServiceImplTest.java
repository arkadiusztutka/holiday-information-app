package com.bluestone.holidayapp.service.external;

import java.time.LocalDate;
import java.util.List;
import java.util.Map;
import java.util.concurrent.CompletableFuture;
import java.util.concurrent.ExecutionException;
import java.util.function.Function;
import java.util.function.Predicate;

import com.bluestone.holidayapp.exception.CountryNotSupportedException;
import com.bluestone.holidayapp.exception.ServiceUnavailableException;
import com.bluestone.holidayapp.request.HolidayRequest;
import com.bluestone.holidayapp.service.external.HolidayExternalServiceImpl.InternalHolidayResponse;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.ArgumentCaptor;
import org.mockito.ArgumentMatchers;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;
import org.springframework.http.HttpStatus;
import org.springframework.test.util.ReflectionTestUtils;
import org.springframework.web.reactive.function.client.ClientResponse;
import org.springframework.web.reactive.function.client.WebClient;
import org.springframework.web.reactive.function.client.WebClient.RequestHeadersSpec;
import org.springframework.web.reactive.function.client.WebClient.RequestHeadersUriSpec;
import org.springframework.web.reactive.function.client.WebClient.ResponseSpec;
import reactor.core.publisher.Mono;

import static com.bluestone.holidayapp.service.external.HolidayExternalServiceImpl.HOLIDAY_API_PATH;
import static org.assertj.core.api.Assertions.assertThat;
import static org.assertj.core.api.Assertions.assertThatThrownBy;
import static org.mockito.ArgumentMatchers.eq;
import static org.mockito.ArgumentMatchers.isNotNull;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

@RunWith(MockitoJUnitRunner.class)
public class HolidayExternalServiceImplTest
{
    private static final String HOLIDAY_API_HOST = "http://localhost";
    @Mock
    private WebClient webClient;

    @InjectMocks
    private HolidayExternalServiceImpl service;

    @Before
    public void setUp()
    {
        ReflectionTestUtils.setField(service, "holidayHost", HOLIDAY_API_HOST);
    }

    @Test
    public void testSearchHolidays() throws ExecutionException, InterruptedException
    {
        var countryCode = "countryCode";
        var year = 2019;
        var localDate = LocalDate.of(2019, 4, 2);
        var holidayRequest = mock(HolidayRequest.class);
        var internalHolidayResponse = mock(InternalHolidayResponse.class);
        var holiday = mock(Holiday.class);
        var holidayMap = Map.of(localDate, List.of(holiday));
        var requestHeadersUriSpec = mock(RequestHeadersUriSpec.class);
        var requestHeadersSpec = mock(RequestHeadersSpec.class);
        var responseSpec1 = mock(ResponseSpec.class);
        var responseSpec2 = mock(ResponseSpec.class);
        ArgumentCaptor<Predicate<HttpStatus>> predicateArgumentCaptor1 = ArgumentCaptor.forClass(Predicate.class);
        ArgumentCaptor<Predicate<HttpStatus>> predicateArgumentCaptor2 = ArgumentCaptor.forClass(Predicate.class);
        ArgumentCaptor<Function<ClientResponse, Mono<? extends Throwable>>> functionArgumentCaptor1 = ArgumentCaptor.forClass(Function.class);
        ArgumentCaptor<Function<ClientResponse, Mono<? extends Throwable>>> functionArgumentCaptor2 = ArgumentCaptor.forClass(Function.class);

        when(webClient.get()).thenReturn(requestHeadersUriSpec);
        when(requestHeadersUriSpec.uri(ArgumentMatchers.notNull(), ArgumentMatchers.<String>notNull(), ArgumentMatchers.<String>notNull())).thenReturn(requestHeadersSpec);
        when(requestHeadersSpec.retrieve()).thenReturn(responseSpec1);
        when(responseSpec1.onStatus(ArgumentMatchers.notNull(), ArgumentMatchers.notNull())).thenReturn(responseSpec2);
        when(responseSpec2.onStatus(ArgumentMatchers.notNull(), ArgumentMatchers.notNull())).thenReturn(responseSpec2);
        when(responseSpec2.bodyToMono(ArgumentMatchers.<Class<InternalHolidayResponse>>notNull()))
                .thenReturn(Mono.just(internalHolidayResponse));

        when(holidayRequest.getCountryCode()).thenReturn(countryCode);
        when(holidayRequest.getYear()).thenReturn(year);
        when(internalHolidayResponse.getHolidays()).thenReturn(holidayMap);

        CompletableFuture<List<Holiday>> result = service.searchHolidays(holidayRequest);

        verify(webClient).get();
        verify(requestHeadersUriSpec).uri(eq(HOLIDAY_API_HOST + HOLIDAY_API_PATH), eq(countryCode), eq(year));
        verify(requestHeadersSpec).retrieve();
        verify(responseSpec1).onStatus(predicateArgumentCaptor1.capture(), functionArgumentCaptor1.capture());
        verify(responseSpec2).onStatus(predicateArgumentCaptor2.capture(), functionArgumentCaptor2.capture());
        verify(responseSpec2).bodyToMono(eq(InternalHolidayResponse.class));
        verify(holidayRequest, times(2)).getCountryCode();
        verify(holidayRequest, times(2)).getYear();

        assertThat(result).isNotNull();
        List<Holiday> holidayList = result.get();
        assertThat(holidayList).containsExactly(holiday);

        assertPredicates(predicateArgumentCaptor1, predicateArgumentCaptor2);
        assertFunctions(functionArgumentCaptor1, functionArgumentCaptor2);
    }

    private void assertFunctions(ArgumentCaptor<Function<ClientResponse, Mono<? extends Throwable>>> functionArgumentCaptor1, ArgumentCaptor<Function<ClientResponse, Mono<? extends Throwable>>> functionArgumentCaptor2)
    {
        Function<ClientResponse, Mono<? extends Throwable>> function1 = functionArgumentCaptor1.getValue();
        Mono<? extends Throwable> apply1 = function1.apply(ClientResponse.create(HttpStatus.BAD_REQUEST).build());
        assertThatThrownBy(apply1::block).isInstanceOf(CountryNotSupportedException.class);

        Function<ClientResponse, Mono<? extends Throwable>> function2 = functionArgumentCaptor2.getValue();
        Mono<? extends Throwable> apply2 = function2.apply(ClientResponse.create(HttpStatus.INTERNAL_SERVER_ERROR).build());
        assertThatThrownBy(apply2::block).isInstanceOf(ServiceUnavailableException.class);
    }

    private void assertPredicates(ArgumentCaptor<Predicate<HttpStatus>> predicateArgumentCaptor1, ArgumentCaptor<Predicate<HttpStatus>> predicateArgumentCaptor2)
    {
        Predicate<HttpStatus> httpStatusPredicate1 = predicateArgumentCaptor1.getValue();
        assertThat(httpStatusPredicate1.test(HttpStatus.BAD_REQUEST)).isTrue();
        assertThat(httpStatusPredicate1.test(HttpStatus.INTERNAL_SERVER_ERROR)).isFalse();
        Predicate<HttpStatus> httpStatusPredicate2 = predicateArgumentCaptor2.getValue();
        assertThat(httpStatusPredicate2.test(HttpStatus.BAD_REQUEST)).isFalse();
        assertThat(httpStatusPredicate2.test(HttpStatus.INTERNAL_SERVER_ERROR)).isTrue();
    }

    @Test
    public void testSearchHolidaysWithCountryNotSupportedException() throws ExecutionException, InterruptedException
    {
        var countryCode = "countryCode";
        var year = 2019;
        var holidayRequest = mock(HolidayRequest.class);
        var requestHeadersUriSpec = mock(RequestHeadersUriSpec.class);
        var requestHeadersSpec = mock(RequestHeadersSpec.class);
        var responseSpec = mock(ResponseSpec.class);
        var countryNotSupportedException = new CountryNotSupportedException(countryCode);

        when(webClient.get()).thenReturn(requestHeadersUriSpec);
        when(requestHeadersUriSpec.uri(ArgumentMatchers.notNull(), ArgumentMatchers.<String>notNull(), ArgumentMatchers.<String>notNull())).thenReturn(requestHeadersSpec);
        when(requestHeadersSpec.retrieve()).thenReturn(responseSpec);
        when(responseSpec.onStatus(ArgumentMatchers.notNull(), ArgumentMatchers.notNull())).thenReturn(responseSpec);
        when(responseSpec.bodyToMono(ArgumentMatchers.<Class<InternalHolidayResponse>>notNull()))
                .thenReturn(Mono.error(countryNotSupportedException));

        when(holidayRequest.getCountryCode()).thenReturn(countryCode);
        when(holidayRequest.getYear()).thenReturn(year);

        assertThatThrownBy(() -> service.searchHolidays(holidayRequest)).isSameAs(countryNotSupportedException);

        verify(webClient).get();
        verify(requestHeadersUriSpec).uri(eq(HOLIDAY_API_HOST + HOLIDAY_API_PATH), eq(countryCode), eq(year));
        verify(requestHeadersSpec).retrieve();
        verify(responseSpec, times(2)).onStatus(isNotNull(), isNotNull());
        verify(responseSpec).bodyToMono(eq(InternalHolidayResponse.class));
        verify(holidayRequest, times(2)).getCountryCode();
        verify(holidayRequest, times(2)).getYear();
    }

    @Test
    public void testSearchHolidaysWithServiceUnavailableException() throws ExecutionException, InterruptedException
    {
        var countryCode = "countryCode";
        var year = 2019;
        var holidayRequest = mock(HolidayRequest.class);
        var requestHeadersUriSpec = mock(RequestHeadersUriSpec.class);
        var requestHeadersSpec = mock(RequestHeadersSpec.class);
        var responseSpec = mock(ResponseSpec.class);
        var serviceUnavailableException = new ServiceUnavailableException();

        when(webClient.get()).thenReturn(requestHeadersUriSpec);
        when(requestHeadersUriSpec.uri(ArgumentMatchers.notNull(), ArgumentMatchers.<String>notNull(), ArgumentMatchers.<String>notNull())).thenReturn(requestHeadersSpec);
        when(requestHeadersSpec.retrieve()).thenReturn(responseSpec);
        when(responseSpec.onStatus(ArgumentMatchers.notNull(), ArgumentMatchers.notNull())).thenReturn(responseSpec);
        when(responseSpec.bodyToMono(ArgumentMatchers.<Class<InternalHolidayResponse>>notNull()))
                .thenReturn(Mono.error(serviceUnavailableException));

        when(holidayRequest.getCountryCode()).thenReturn(countryCode);
        when(holidayRequest.getYear()).thenReturn(year);

        assertThatThrownBy(() -> service.searchHolidays(holidayRequest)).isSameAs(serviceUnavailableException);

        verify(webClient).get();
        verify(requestHeadersUriSpec).uri(eq(HOLIDAY_API_HOST + HOLIDAY_API_PATH), eq(countryCode), eq(year));
        verify(requestHeadersSpec).retrieve();
        verify(responseSpec, times(2)).onStatus(isNotNull(), isNotNull());
        verify(responseSpec).bodyToMono(eq(InternalHolidayResponse.class));
        verify(holidayRequest, times(2)).getCountryCode();
        verify(holidayRequest, times(2)).getYear();
    }

}