package com.bluestone.holidayapp.controller;

import java.util.concurrent.ExecutionException;

import com.bluestone.holidayapp.request.NextSameDayHolidayRequest;
import com.bluestone.holidayapp.request.NextSameDayHolidayRequestDTO;
import com.bluestone.holidayapp.request.NextSameDayHolidayRequestMapper;
import com.bluestone.holidayapp.response.NextSameDayHolidayResponse;
import com.bluestone.holidayapp.response.NextSameDayHolidayResponseDTO;
import com.bluestone.holidayapp.response.NextSameDayHolidayResponseDTOMapper;
import com.bluestone.holidayapp.service.HolidayService;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;

import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.same;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

@RunWith(MockitoJUnitRunner.class)
public class HolidayControllerTest
{
    @Mock
    private NextSameDayHolidayRequestMapper nextSameDayHolidayRequestMapper;

    @Mock
    private NextSameDayHolidayResponseDTOMapper nextSameDayHolidayResponseDTOMapper;

    @Mock
    private HolidayService holidayService;

    private HolidayController controller;

    @Before
    public void setUp()
    {
        controller = new HolidayController(nextSameDayHolidayRequestMapper,
            nextSameDayHolidayResponseDTOMapper, holidayService);
    }

    @Test
    public void testGetNextSameDayHoliday() throws ExecutionException, InterruptedException
    {
        var requestDTO = mock(NextSameDayHolidayRequestDTO.class);
        var request = mock(NextSameDayHolidayRequest.class);
        var response = mock(NextSameDayHolidayResponse.class);
        var responseDTO = mock(NextSameDayHolidayResponseDTO.class);

        when(nextSameDayHolidayRequestMapper.map(any(NextSameDayHolidayRequestDTO.class))).thenReturn(request);
        when(nextSameDayHolidayResponseDTOMapper.map(any(NextSameDayHolidayResponse.class))).thenReturn(responseDTO);
        when(holidayService.getNextSameDayHoliday(any(NextSameDayHolidayRequest.class))).thenReturn(response);

        NextSameDayHolidayResponseDTO result = controller.getNextSameDayHoliday(requestDTO);

        verify(nextSameDayHolidayRequestMapper).map(same(requestDTO));
        verify(holidayService).getNextSameDayHoliday(same(request));
        verify(nextSameDayHolidayResponseDTOMapper).map(same(response));

        assertThat(result).isSameAs(responseDTO);
    }
}