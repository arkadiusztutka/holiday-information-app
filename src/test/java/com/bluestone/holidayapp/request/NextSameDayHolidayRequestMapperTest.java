package com.bluestone.holidayapp.request;

import java.time.LocalDate;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

@RunWith(SpringJUnit4ClassRunner.class)
public class NextSameDayHolidayRequestMapperTest
{
    private NextSameDayHolidayRequestMapper mapper = new NextSameDayHolidayRequestMapper();

    @Test
    public void testMap()
    {
        var maxYears = "10";
        var date = "2019-04-02";
        var country1 = "country1";
        var country2 = "country2";
        var requestDTO = mock(NextSameDayHolidayRequestDTO.class);

        when(requestDTO.getMaxYears()).thenReturn(maxYears);
        when(requestDTO.getDate()).thenReturn(date);
        when(requestDTO.getCountryCode1()).thenReturn(country1);
        when(requestDTO.getCountryCode2()).thenReturn(country2);

        NextSameDayHolidayRequest result = mapper.map(requestDTO);

        verify(requestDTO, times(2)).getMaxYears();
        verify(requestDTO).getCountryCode1();
        verify(requestDTO).getCountryCode2();
        verify(requestDTO).getDate();

        assertThat(result).isNotNull();
        assertThat(result.getMaxYears()).isEqualTo(10);
        assertThat(result.getDate()).isEqualTo(LocalDate.parse(date));
        assertThat(result.getCountryCode1()).isEqualTo(country1);
        assertThat(result.getCountryCode2()).isEqualTo(country2);
    }

    @Test
    public void testMapMinimal()
    {
        var date = "2019-04-02";
        var country1 = "country1";
        var country2 = "country2";
        var requestDTO = mock(NextSameDayHolidayRequestDTO.class);

        when(requestDTO.getMaxYears()).thenReturn(null);
        when(requestDTO.getDate()).thenReturn(date);
        when(requestDTO.getCountryCode1()).thenReturn(country1);
        when(requestDTO.getCountryCode2()).thenReturn(country2);

        NextSameDayHolidayRequest result = mapper.map(requestDTO);

        verify(requestDTO).getMaxYears();
        verify(requestDTO).getCountryCode1();
        verify(requestDTO).getCountryCode2();
        verify(requestDTO).getDate();

        assertThat(result).isNotNull();
        assertThat(result.getMaxYears()).isNull();
        assertThat(result.getDate()).isEqualTo(LocalDate.parse(date));
        assertThat(result.getCountryCode1()).isEqualTo(country1);
        assertThat(result.getCountryCode2()).isEqualTo(country2);
    }
}