package com.bluestone.holidayapp;

import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.List;
import java.util.concurrent.CompletableFuture;

import com.bluestone.holidayapp.request.NextSameDayHolidayRequestDTO;
import com.bluestone.holidayapp.response.NextSameDayHolidayResponseDTO;
import com.bluestone.holidayapp.service.external.Holiday;
import com.bluestone.holidayapp.service.external.HolidayExternalService;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.ArgumentMatchers;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.context.SpringBootTest.WebEnvironment;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.boot.test.web.client.TestRestTemplate;
import org.springframework.boot.web.server.LocalServerPort;
import org.springframework.test.context.TestPropertySource;
import org.springframework.test.context.junit4.SpringRunner;

import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.Mockito.when;

@RunWith(SpringRunner.class)
@SpringBootTest(webEnvironment = WebEnvironment.RANDOM_PORT)
@TestPropertySource(locations = "classpath:application.test.properties")
public class HolidayappApplicationIntegrationTest
{
    private static final String REST_METHOD_URL = "/getNextSameDayHoliday";

    @LocalServerPort
    private int port;

    @MockBean
    private HolidayExternalService holidayExternalService;

    @Autowired
    private TestRestTemplate restTemplate;

    @Test
    @SuppressWarnings("unchecked")
    public void testGetNextSameDayHoliday() throws Exception
    {
        var countryCode1 = "PL";
        var countryCode2 = "GB";
        var date = LocalDate.of(2019, 1, 2);
        var dateString = date.format(DateTimeFormatter.ISO_LOCAL_DATE);

        var requestDTO = new NextSameDayHolidayRequestDTO();
        requestDTO.setCountryCode1(countryCode1);
        requestDTO.setCountryCode2(countryCode2);
        requestDTO.setDate(dateString);

        var localDate1 = LocalDate.of(2019, 1, 1);
        var name1 = "name1";
        var holiday1 = createHoliday(localDate1, countryCode1, name1);
        var localDate2 = LocalDate.of(2019, 5, 28);
        var name2 = "name2";
        var holiday2 = createHoliday(localDate2, countryCode1, name2);
        var localDate3 = LocalDate.of(2019, 5, 1);
        var name3 = "name3";
        var holiday3 = createHoliday(localDate3, countryCode1, name3);
        var localDate4 = LocalDate.of(2019, 6, 1);
        var name4 = "name4";
        var holiday4 = createHoliday(localDate4, countryCode2, name4);
        var localDate5 = LocalDate.of(2019, 1, 1);
        var name5 = "name5";
        var holiday5 = createHoliday(localDate5, countryCode2, name5);
        var localDate6 = LocalDate.of(2019, 7, 15);
        var name6 = "name6";
        var holiday6 = createHoliday(localDate6, countryCode2, name6);
        var localDate7 = LocalDate.of(2019, 5, 28);
        var name7 = "name7";
        var holiday7 = createHoliday(localDate7, countryCode2, name7);

        var holidays1 = List.of(holiday1, holiday2, holiday3);
        var listCompletableFuture1 = CompletableFuture.completedFuture(holidays1);
        var holidays2 = List.of(holiday4, holiday5, holiday6, holiday7);
        var listCompletableFuture2 = CompletableFuture.completedFuture(holidays2);
        when(holidayExternalService.searchHolidays(ArgumentMatchers.isNotNull()))
                .thenReturn(listCompletableFuture1, listCompletableFuture2);

        String results = restTemplate.getForObject("http://localhost:" + port +
                        REST_METHOD_URL + "?date=" + dateString + "&countryCode1=" + countryCode1 + "&countryCode2=" + countryCode2,
                String.class, requestDTO);

        assertThat(results).isNotNull();
        var mapper = new ObjectMapper();
        var responseDTO = mapper.readValue(results, NextSameDayHolidayResponseDTO.class);
        assertThat(responseDTO.getDate()).isEqualTo(localDate7.format(DateTimeFormatter.ISO_LOCAL_DATE));
        assertThat(responseDTO.getHolidayCountry1()).isEqualTo(name2);
        assertThat(responseDTO.getHolidayCountry2()).isEqualTo(name7);
    }

    private static Holiday createHoliday(LocalDate localDate, String country, String name)
    {
        var holiday = new Holiday();
        holiday.setDate(localDate);
        holiday.setCountry(country);
        holiday.setName(name);
        return holiday;
    }

}
