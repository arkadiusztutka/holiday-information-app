package com.bluestone.holidayapp.exception;

import java.util.Locale;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.TimeoutException;
import javax.servlet.http.HttpServletRequest;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.ArgumentCaptor;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;
import org.springframework.context.MessageSource;
import org.springframework.http.HttpStatus;
import org.springframework.validation.BindException;
import org.springframework.validation.FieldError;

import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.anyString;
import static org.mockito.ArgumentMatchers.eq;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

@RunWith(MockitoJUnitRunner.class)
public class GlobalControllerExceptionHandlerTest
{
    @Mock
    private MessageSource messageSource;

    @InjectMocks
    private GlobalControllerExceptionHandler handler;

    @Test
    public void testHandleExecutionExceptionWithExecutionException() throws Exception
    {
        var message = "message";
        var exception = mock(ExecutionException.class);
        var arrayCaptor = ArgumentCaptor.forClass(Object[].class);
        var localeCaptor = ArgumentCaptor.forClass(Locale.class);
        var httpServletRequest = mock(HttpServletRequest.class);

        when(messageSource.getMessage(anyString(), any(Object[].class), any(Locale.class))).thenReturn(message);

        ErrorResponse result = handler.handleExecutionException(httpServletRequest, exception);

        verify(messageSource).getMessage(eq("error.unknown.msg"), arrayCaptor.capture(), localeCaptor.capture());

        assertThat(result).isNotNull();
        assertThat(result.getStatus()).isEqualTo(HttpStatus.INTERNAL_SERVER_ERROR);
        assertThat(result.getErrorMessage()).isEqualTo(message);

        Object[] objects = arrayCaptor.getValue();
        assertThat(objects.length).isEqualTo(0);
        Locale locale = localeCaptor.getValue();
        assertThat(locale).isEqualTo(Locale.ENGLISH);
    }

    @Test
    public void testHandleExecutionExceptionWithCauseCountryNotSupportedException() throws Exception
    {
        var message = "message";
        var countryCode = "countryCode";
        var exception = mock(ExecutionException.class);
        var causeException = mock(CountryNotSupportedException.class);
        var arrayCaptor = ArgumentCaptor.forClass(Object[].class);
        var localeCaptor = ArgumentCaptor.forClass(Locale.class);
        var httpServletRequest = mock(HttpServletRequest.class);

        when(exception.getCause()).thenReturn(causeException);
        when(causeException.getCountryCode()).thenReturn(countryCode);
        when(messageSource.getMessage(anyString(), any(Object[].class), any(Locale.class))).thenReturn(message);

        ErrorResponse result = handler.handleExecutionException(httpServletRequest, exception);

        verify(messageSource).getMessage(eq("error.countrynotsupported.msg"), arrayCaptor.capture(), localeCaptor.capture());
        verify(exception, times(2)).getCause();
        verify(causeException).getCountryCode();

        assertThat(result).isNotNull();
        assertThat(result.getStatus()).isEqualTo(HttpStatus.BAD_REQUEST);
        assertThat(result.getErrorMessage()).isEqualTo(message);

        Object[] objects = arrayCaptor.getValue();
        assertThat(objects.length).isEqualTo(1);
        assertThat(objects[0]).isEqualTo(countryCode);

        Locale locale = localeCaptor.getValue();
        assertThat(locale).isEqualTo(Locale.ENGLISH);
    }

    @Test
    public void testHandleExecutionExceptionWithCauseServiceUnavailableException() throws Exception
    {
        var message = "message";
        var exception = mock(ExecutionException.class);
        var causeException = mock(ServiceUnavailableException.class);
        var arrayCaptor = ArgumentCaptor.forClass(Object[].class);
        var localeCaptor = ArgumentCaptor.forClass(Locale.class);
        var httpServletRequest = mock(HttpServletRequest.class);

        when(exception.getCause()).thenReturn(causeException);
        when(messageSource.getMessage(anyString(), any(Object[].class), any(Locale.class))).thenReturn(message);

        ErrorResponse result = handler.handleExecutionException(httpServletRequest, exception);

        verify(messageSource).getMessage(eq("error.serviceunavailable.msg"), arrayCaptor.capture(), localeCaptor.capture());
        verify(exception, times(2)).getCause();

        assertThat(result).isNotNull();
        assertThat(result.getStatus()).isEqualTo(HttpStatus.INTERNAL_SERVER_ERROR);
        assertThat(result.getErrorMessage()).isEqualTo(message);

        Object[] objects = arrayCaptor.getValue();
        assertThat(objects.length).isEqualTo(0);

        Locale locale = localeCaptor.getValue();
        assertThat(locale).isEqualTo(Locale.ENGLISH);
    }

    @Test
    public void testHandleExecutionExceptionWithCauseTimeoutException() throws Exception
    {
        var message = "message";
        var exception = mock(ExecutionException.class);
        var causeException = mock(TimeoutException.class);
        var arrayCaptor = ArgumentCaptor.forClass(Object[].class);
        var localeCaptor = ArgumentCaptor.forClass(Locale.class);
        var httpServletRequest = mock(HttpServletRequest.class);

        when(exception.getCause()).thenReturn(causeException);
        when(messageSource.getMessage(anyString(), any(Object[].class), any(Locale.class))).thenReturn(message);

        ErrorResponse result = handler.handleExecutionException(httpServletRequest, exception);

        verify(messageSource).getMessage(eq("error.serviceunavailable.msg"), arrayCaptor.capture(), localeCaptor.capture());
        verify(exception, times(2)).getCause();

        assertThat(result).isNotNull();
        assertThat(result.getStatus()).isEqualTo(HttpStatus.INTERNAL_SERVER_ERROR);
        assertThat(result.getErrorMessage()).isEqualTo(message);

        Object[] objects = arrayCaptor.getValue();
        assertThat(objects.length).isEqualTo(0);

        Locale locale = localeCaptor.getValue();
        assertThat(locale).isEqualTo(Locale.ENGLISH);
    }

    @Test
    public void testHandleValidationException() throws Exception
    {
        var message = "message";
        var exception = mock(BindException.class);
        var httpServletRequest = mock(HttpServletRequest.class);
        var filedError = mock(FieldError.class);

        when(exception.getFieldError()).thenReturn(filedError);
        when(filedError.getDefaultMessage()).thenReturn(message);

        ErrorResponse result = handler.handleValidationException(httpServletRequest, exception);

        verify(exception).getFieldError();
        verify(filedError).getDefaultMessage();

        assertThat(result).isNotNull();
        assertThat(result.getStatus()).isEqualTo(HttpStatus.BAD_REQUEST);
        assertThat(result.getErrorMessage()).isEqualTo(message);
    }

    @Test
    public void testHandleNextSameDayHolidayNotFoundException() throws Exception
    {
        var message = "message";
        var years = 2;
        var exception = mock(NextSameDayHolidayNotFoundException.class);
        var arrayCaptor = ArgumentCaptor.forClass(Object[].class);
        var localeCaptor = ArgumentCaptor.forClass(Locale.class);
        var httpServletRequest = mock(HttpServletRequest.class);

        when(exception.getYears()).thenReturn(years);
        when(messageSource.getMessage(anyString(), any(Object[].class), any(Locale.class))).thenReturn(message);

        ErrorResponse result = handler.handleNextSameDayHolidayNotFoundException(httpServletRequest, exception);

        verify(messageSource).getMessage(eq("error.nextsamedayholidaynotfound.msg"),
                arrayCaptor.capture(), localeCaptor.capture());
        verify(exception).getYears();

        assertThat(result).isNotNull();
        assertThat(result.getStatus()).isEqualTo(HttpStatus.NOT_FOUND);
        assertThat(result.getErrorMessage()).isEqualTo(message);

        Object[] objects = arrayCaptor.getValue();
        assertThat(objects.length).isEqualTo(1);
        assertThat(objects[0]).isEqualTo(years);

        Locale locale = localeCaptor.getValue();
        assertThat(locale).isEqualTo(Locale.ENGLISH);
    }

    @Test
    public void testHandleUnknownException() throws Exception
    {
        var message = "message";
        var exception = mock(IllegalArgumentException.class);
        var arrayCaptor = ArgumentCaptor.forClass(Object[].class);
        var localeCaptor = ArgumentCaptor.forClass(Locale.class);

        when(messageSource.getMessage(anyString(), any(Object[].class), any(Locale.class))).thenReturn(message);

        ErrorResponse result = handler.handleUnknownException(exception);

        verify(messageSource).getMessage(eq("error.unknown.msg"), arrayCaptor.capture(), localeCaptor.capture());

        assertThat(result).isNotNull();
        assertThat(result.getStatus()).isEqualTo(HttpStatus.INTERNAL_SERVER_ERROR);
        assertThat(result.getErrorMessage()).isEqualTo(message);

        Object[] objects = arrayCaptor.getValue();
        assertThat(objects.length).isEqualTo(0);
        Locale locale = localeCaptor.getValue();
        assertThat(locale).isEqualTo(Locale.ENGLISH);
    }
}