package com.bluestone.holidayapp.response;

import java.time.LocalDate;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

@RunWith(SpringJUnit4ClassRunner.class)
public class NextSameDayHolidayResponseDTOMapperTest
{
    private NextSameDayHolidayResponseDTOMapper mapper = new NextSameDayHolidayResponseDTOMapper();

    @Test
    public void testMap()
    {
        var dateString = "2019-04-02";
        var date = LocalDate.parse(dateString);
        var name1 = "name1";
        var name2 = "name2";
        var response = mock(NextSameDayHolidayResponse.class);

        when(response.getDate()).thenReturn(date);
        when(response.getName1()).thenReturn(name1);
        when(response.getName2()).thenReturn(name2);

        NextSameDayHolidayResponseDTO result = mapper.map(response);

        verify(response).getDate();
        verify(response).getName1();
        verify(response).getName2();

        assertThat(result).isNotNull();
        assertThat(result.getDate()).isEqualTo(dateString);
        assertThat(result.getHolidayCountry1()).isEqualTo(name1);
        assertThat(result.getHolidayCountry2()).isEqualTo(name2);

    }

}