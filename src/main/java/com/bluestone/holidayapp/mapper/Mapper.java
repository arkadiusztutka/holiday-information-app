package com.bluestone.holidayapp.mapper;

/**
 * Maps object from one type to other
 *
 * @param <S>
 *     Type of object before mapped
 * @param <T>
 *     Type of object to map
 */
@FunctionalInterface
public interface Mapper<S,T>
{
    T map(S source);
}
