package com.bluestone.holidayapp.response;

import java.time.LocalDate;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
@AllArgsConstructor
public class NextSameDayHolidayResponse
{
    private final LocalDate date;
    private final String name1;
    private final String name2;
}
