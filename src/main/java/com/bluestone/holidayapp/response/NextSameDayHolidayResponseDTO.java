package com.bluestone.holidayapp.response;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class NextSameDayHolidayResponseDTO
{
    private String date;
    private String holidayCountry1;
    private String holidayCountry2;
}
