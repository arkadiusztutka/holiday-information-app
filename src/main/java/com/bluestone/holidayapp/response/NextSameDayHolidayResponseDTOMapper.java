package com.bluestone.holidayapp.response;

import java.time.format.DateTimeFormatter;

import com.bluestone.holidayapp.mapper.Mapper;
import org.springframework.stereotype.Component;

@Component
public class NextSameDayHolidayResponseDTOMapper implements Mapper<NextSameDayHolidayResponse, NextSameDayHolidayResponseDTO>
{
    @Override
    public NextSameDayHolidayResponseDTO map(NextSameDayHolidayResponse source)
    {
        var target = new NextSameDayHolidayResponseDTO();
        target.setDate(source.getDate().format(DateTimeFormatter.ISO_LOCAL_DATE));
        target.setHolidayCountry1(source.getName1());
        target.setHolidayCountry2(source.getName2());
        return target;
    }
}
