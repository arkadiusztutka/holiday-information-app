package com.bluestone.holidayapp;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class HolidayappApplication
{

	public static void main(String[] args) {
		SpringApplication.run(HolidayappApplication.class, args);
	}

}
