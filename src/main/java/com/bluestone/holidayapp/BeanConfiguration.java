package com.bluestone.holidayapp;

import java.util.concurrent.Executor;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.scheduling.annotation.EnableAsync;
import org.springframework.scheduling.concurrent.ThreadPoolTaskExecutor;
import org.springframework.web.reactive.function.client.WebClient;

@Configuration
@EnableAsync(proxyTargetClass=true)
public class BeanConfiguration
{
   private static final String THREAD_NAME_PREFIX = "AppExecutor-";

   @Value("${executor.corePoolSize}")
   private int corePoolSize;

   @Value("${executor.maxPoolSize}")
   private int maxPoolSize;

   @Value("${executor.queueCapacity}")
   private int queueCapacity;

   @Bean("executor")
   Executor asyncExecutor()
   {
      var executor = new ThreadPoolTaskExecutor();
      executor.setCorePoolSize(corePoolSize);
      executor.setMaxPoolSize(maxPoolSize);
      executor.setQueueCapacity(queueCapacity);
      executor.setThreadNamePrefix(THREAD_NAME_PREFIX);
      executor.initialize();
      return executor;
   }

   @Bean
   WebClient createWebClient()
   {
      return WebClient.create();
   }

}
