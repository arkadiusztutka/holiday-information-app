package com.bluestone.holidayapp.service;

import java.util.concurrent.ExecutionException;

import com.bluestone.holidayapp.exception.NextSameDayHolidayNotFoundException;
import com.bluestone.holidayapp.request.NextSameDayHolidayRequest;
import com.bluestone.holidayapp.response.NextSameDayHolidayResponse;

public interface HolidayService
{
    /**
     * Get next holiday that will be in same day for given countries
     * @param request
     *      criteria to search holiday
     * @return
     * @throws ExecutionException
     *      when one or both tasks have problems with completion
     * @throws InterruptedException
     *      when one of the tasks is interrupted
     * @throws NextSameDayHolidayNotFoundException
     *      when holiday is not found
     */
    NextSameDayHolidayResponse getNextSameDayHoliday(NextSameDayHolidayRequest request)
            throws ExecutionException, InterruptedException, NextSameDayHolidayNotFoundException;
}
