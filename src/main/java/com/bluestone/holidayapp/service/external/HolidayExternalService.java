package com.bluestone.holidayapp.service.external;

import java.util.List;
import java.util.concurrent.CompletableFuture;

import com.bluestone.holidayapp.request.HolidayRequest;

public interface HolidayExternalService
{
    /**
     * Searches for holiday in external service
     * @param request
     *      criteria to search
     * @return
     *      {@link CompletableFuture} with list of holidays
     */
    CompletableFuture<List<Holiday>> searchHolidays(HolidayRequest request);
}
