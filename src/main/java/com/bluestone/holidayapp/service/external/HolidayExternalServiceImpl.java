package com.bluestone.holidayapp.service.external;

import java.time.LocalDate;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.CompletableFuture;
import java.util.stream.Collectors;

import com.bluestone.holidayapp.exception.CountryNotSupportedException;
import com.bluestone.holidayapp.exception.ServiceUnavailableException;
import com.bluestone.holidayapp.request.HolidayRequest;
import lombok.Getter;
import lombok.Setter;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpStatus;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Service;
import org.springframework.web.reactive.function.client.WebClient;
import reactor.core.publisher.Mono;

@Service
@Slf4j
public class HolidayExternalServiceImpl implements HolidayExternalService
{
    static final String HOLIDAY_API_PATH = "/holidays?country={country}&year={year}";

    @Value("${holiday.service.host}")
    private String holidayHost;

    private final WebClient webClient;

    @Autowired
    public HolidayExternalServiceImpl(WebClient webClient) {this.webClient = webClient;}

    @Async("executor")
    @Override
    public CompletableFuture<List<Holiday>> searchHolidays(HolidayRequest request)
    {
        log.info("Getting holidays for country {} and year {}", request.getCountryCode(), request.getYear());
        InternalHolidayResponse holidayResponse = searchHolidaysInProvider(request);
        List<Holiday> holidays = getHolidays(holidayResponse);
        return CompletableFuture.completedFuture(holidays);
    }

    private InternalHolidayResponse searchHolidaysInProvider(HolidayRequest request)
    {
        String countryCode = request.getCountryCode();
        return webClient.get()
                .uri(holidayHost + HOLIDAY_API_PATH, countryCode, request.getYear())
                .retrieve()
                .onStatus(HttpStatus::is4xxClientError, clientResponse -> handleCountryNotSupportedException(new CountryNotSupportedException(countryCode)))
                .onStatus(HttpStatus::is5xxServerError, clientResponse -> handleServiceNotAvailableException(new ServiceUnavailableException()))
                .bodyToMono(InternalHolidayResponse.class)
                .block();
    }

    private Mono<Throwable> handleCountryNotSupportedException(CountryNotSupportedException exception)
    {
        log.error(String.format("Given country '%s' is not supported", exception.getCountryCode()), exception);
        return Mono.error(exception);
    }

    private Mono<Throwable> handleServiceNotAvailableException(ServiceUnavailableException exception)
    {
        log.error("Service is not available", exception);
        return Mono.error(exception);
    }

    private List<Holiday> getHolidays(InternalHolidayResponse holidayResponses)
    {
        return holidayResponses.getHolidays()
                .values()
                .stream()
                .flatMap(List::stream)
                .collect(Collectors.toList());
    }

    @Getter
    @Setter
    static class InternalHolidayResponse
    {
        private Map<LocalDate, List<Holiday>> holidays = new HashMap<>();
    }

}
