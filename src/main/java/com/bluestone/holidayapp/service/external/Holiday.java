package com.bluestone.holidayapp.service.external;

import java.time.LocalDate;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class Holiday
{
    private String name;
    private String country;
    private LocalDate date;
}
