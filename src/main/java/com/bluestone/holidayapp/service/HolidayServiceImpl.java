package com.bluestone.holidayapp.service;

import java.time.LocalDate;
import java.util.Comparator;
import java.util.List;
import java.util.Optional;
import java.util.concurrent.CompletableFuture;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.TimeUnit;
import java.util.stream.Collectors;

import com.bluestone.holidayapp.exception.NextSameDayHolidayNotFoundException;
import com.bluestone.holidayapp.request.HolidayRequest;
import com.bluestone.holidayapp.request.NextSameDayHolidayRequest;
import com.bluestone.holidayapp.response.NextSameDayHolidayResponse;
import com.bluestone.holidayapp.service.external.Holiday;
import com.bluestone.holidayapp.service.external.HolidayExternalService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

@Service
@Slf4j
public class HolidayServiceImpl implements HolidayService
{
    private HolidayExternalService holidayExternalService;

    @Value("${holiday.service.timeout}")
    private int timeout;

    @Value("${holiday.search.maxyears}")
    private int defaultMaxYears;

    @Autowired
    public HolidayServiceImpl(HolidayExternalService holidayExternalService)
    {
        this.holidayExternalService = holidayExternalService;
    }

    @Override
    public NextSameDayHolidayResponse getNextSameDayHoliday(NextSameDayHolidayRequest request)
            throws ExecutionException, InterruptedException, NextSameDayHolidayNotFoundException
    {
        LocalDate date = request.getDate();
        int year = date.getYear();
        int maxYears = request.getMaxYears() != null ? request.getMaxYears() : defaultMaxYears;
        Optional<NextSameDayHolidayResponse> holidayResponseOptional = Optional.empty();
        for(int i=year; i <= year + maxYears && holidayResponseOptional.isEmpty(); i++)
        {
            log.info("Searching next same holiday for year: {}", i);
            var holidaysForCountry1Request = new HolidayRequest(request.getCountryCode1(), i);
            var holidaysForCountry2Request = new HolidayRequest(request.getCountryCode2(), i);
            List<Holiday> holidays = searchHolidays(holidaysForCountry1Request, holidaysForCountry2Request);
            holidays = holidays.stream()
                    .filter(h -> h.getDate().isAfter(date))
                    .sorted(Comparator.comparing(Holiday::getDate))
                    .collect(Collectors.toList());
            holidayResponseOptional = findNextSameDayHoliday(holidays);
        }
        if(holidayResponseOptional.isEmpty())
        {
            log.error("Not found next same day holiday for countries: {}, {}. Search for {} years, started in {}",
                    request.getCountryCode1(), request.getCountryCode2(), maxYears, year);
            throw new NextSameDayHolidayNotFoundException(maxYears);
        }
        return holidayResponseOptional.get();
    }

    private List<Holiday> searchHolidays(HolidayRequest holidaysForCountry1Request, HolidayRequest holidaysForCountry2Request) throws ExecutionException, InterruptedException
    {
        try
            {
                List<CompletableFuture<List<Holiday>>> holidayCompletableFutures = List.of(holidayExternalService.searchHolidays(holidaysForCountry1Request),
                        holidayExternalService.searchHolidays(holidaysForCountry2Request));
                CompletableFuture<List<Holiday>> allDoneCompletableFuture = CompletableFuture.allOf(holidayCompletableFutures.toArray(new CompletableFuture[2]))
                        .thenApply(v -> holidayCompletableFutures.stream()
                                .map(CompletableFuture::join)
                                .flatMap(List::stream)
                                .collect(Collectors.toList()));
                return allDoneCompletableFuture.orTimeout(timeout, TimeUnit.SECONDS)
                        .get();
            } catch (InterruptedException exception)
            {
                log.error("Thread couldn't finnish his work due to some problems", exception);
                throw exception;
            } catch (ExecutionException exception)
            {
                log.error("Couldn't get results from thread tasks because of error in one of them or both", exception);
                throw exception;
            }
    }

    private Optional<NextSameDayHolidayResponse> findNextSameDayHoliday(List<Holiday> holidays)
    {
        for(int i=0; i<holidays.size()-1; i++)
        {
            var holiday1 = holidays.get(i);
            var holiday2 = holidays.get(i+1);
            if(holiday1.getDate().equals(holiday2.getDate()) && !holiday1.getCountry().equals(holiday2.getCountry()))
            {
                var response = new NextSameDayHolidayResponse(holiday1.getDate(), holiday1.getName(), holiday2.getName());
                return Optional.of(response);
            }
        }
        return Optional.empty();
    }

}
