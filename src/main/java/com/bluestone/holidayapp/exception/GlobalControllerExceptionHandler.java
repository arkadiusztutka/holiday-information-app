package com.bluestone.holidayapp.exception;

import java.util.Locale;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.TimeoutException;
import javax.servlet.http.HttpServletRequest;

import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.MessageSource;
import org.springframework.http.HttpStatus;
import org.springframework.validation.BindException;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestControllerAdvice;

@RestControllerAdvice
@Slf4j
public class GlobalControllerExceptionHandler
{
    private final MessageSource messageSource;

    @Autowired
    public GlobalControllerExceptionHandler(MessageSource messageSource) {this.messageSource = messageSource;}

    @ExceptionHandler(value = ExecutionException.class)
    @ResponseStatus(HttpStatus.BAD_REQUEST)
    public ErrorResponse handleExecutionException(HttpServletRequest req, ExecutionException ex)
    {
        log.error("Error with getting data from external service", ex);
        Throwable cause = ex.getCause();
        if(cause instanceof CountryNotSupportedException)
        {
            return ErrorResponse.create(HttpStatus.BAD_REQUEST, getMessage("error.countrynotsupported.msg", ((CountryNotSupportedException) cause).getCountryCode()));
        }
        if(cause instanceof ServiceUnavailableException || cause instanceof TimeoutException)
        {
            return ErrorResponse.create(HttpStatus.INTERNAL_SERVER_ERROR, getMessage("error.serviceunavailable.msg"));
        }
        return handleUnknownException(ex);
    }

    @ExceptionHandler(value = BindException.class)
    @ResponseStatus(HttpStatus.BAD_REQUEST)
    public ErrorResponse handleValidationException(HttpServletRequest req, BindException ex)
    {
        String defaultMessage = ex.getFieldError().getDefaultMessage();
        return ErrorResponse.create(HttpStatus.BAD_REQUEST, defaultMessage);
    }

    @ExceptionHandler(value = NextSameDayHolidayNotFoundException.class)
    @ResponseStatus(HttpStatus.BAD_REQUEST)
    public ErrorResponse handleNextSameDayHolidayNotFoundException(HttpServletRequest req, NextSameDayHolidayNotFoundException ex)
    {
        return ErrorResponse.create(HttpStatus.NOT_FOUND, getMessage("error.nextsamedayholidaynotfound.msg", ex.getYears()));
    }

    @ExceptionHandler(value = Exception.class)
    @ResponseStatus(HttpStatus.INTERNAL_SERVER_ERROR)
    public ErrorResponse handleUnknownException(Exception ex)
    {
        log.error("Internal application error", ex);
        return ErrorResponse.create(HttpStatus.INTERNAL_SERVER_ERROR, getMessage("error.unknown.msg"));
    }

    private String getMessage(String key, Object... objects)
    {
        return messageSource.getMessage(key, objects, Locale.ENGLISH);
    }
}
