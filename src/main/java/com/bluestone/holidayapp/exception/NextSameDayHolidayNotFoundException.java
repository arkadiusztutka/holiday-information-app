package com.bluestone.holidayapp.exception;

import lombok.Getter;

@Getter
public class NextSameDayHolidayNotFoundException extends RuntimeException
{
    private int years;

    public NextSameDayHolidayNotFoundException(int years)
    {
        super();
        this.years = years;
    }
}
