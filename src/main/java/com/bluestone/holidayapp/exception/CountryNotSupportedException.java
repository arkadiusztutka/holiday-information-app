package com.bluestone.holidayapp.exception;

import lombok.Getter;

@Getter
public class CountryNotSupportedException extends RuntimeException
{
    private String countryCode;

    public CountryNotSupportedException(String countryCode)
    {
        super();
        this.countryCode = countryCode;
    }
}
