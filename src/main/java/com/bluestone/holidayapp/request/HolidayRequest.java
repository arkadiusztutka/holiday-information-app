package com.bluestone.holidayapp.request;

import lombok.Getter;
import lombok.RequiredArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@RequiredArgsConstructor
public class HolidayRequest
{
    private final String countryCode;
    private final int year;
}
