package com.bluestone.holidayapp.request;

import java.time.LocalDate;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class NextSameDayHolidayRequest
{
    private String countryCode1;
    private String countryCode2;
    private LocalDate date;
    private Integer maxYears;
}
