package com.bluestone.holidayapp.request;

import javax.validation.constraints.Min;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.Pattern;

import lombok.Getter;
import lombok.Setter;

@Setter
@Getter
public class NextSameDayHolidayRequestDTO
{
    @Pattern(regexp = "^\\d{4}\\-(0[1-9]|1[012])\\-(0[1-9]|[12][0-9]|3[01])$", message = "{validation.date.pattern.error}")
    @NotBlank(message = "{validation.missing.date}")
    private String date;

    @NotBlank(message = "{validation.missing.countrycode1}")
    private String countryCode1;

    @NotBlank(message = "{validation.missing.countrycode2}")
    private String countryCode2;

    @Min(value = 0, message = "{validation.minimum.maxYears}")
    private String maxYears;
}
