package com.bluestone.holidayapp.request;

import java.time.LocalDate;

import com.bluestone.holidayapp.mapper.Mapper;
import org.springframework.stereotype.Component;

@Component
public class NextSameDayHolidayRequestMapper implements Mapper<NextSameDayHolidayRequestDTO, NextSameDayHolidayRequest>
{
    @Override
    public NextSameDayHolidayRequest map(NextSameDayHolidayRequestDTO source)
    {
        var target = new NextSameDayHolidayRequest();
        var localDate = LocalDate.parse(source.getDate());
        target.setDate(localDate);
        target.setCountryCode1(source.getCountryCode1());
        target.setCountryCode2(source.getCountryCode2());
        if(source.getMaxYears() != null)
        {
            target.setMaxYears(Integer.valueOf(source.getMaxYears()));
        }
        return target;
    }
}
