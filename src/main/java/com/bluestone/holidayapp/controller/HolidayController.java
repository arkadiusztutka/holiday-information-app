package com.bluestone.holidayapp.controller;

import java.util.concurrent.ExecutionException;
import javax.validation.Valid;

import com.bluestone.holidayapp.exception.NextSameDayHolidayNotFoundException;
import com.bluestone.holidayapp.mapper.Mapper;
import com.bluestone.holidayapp.request.NextSameDayHolidayRequest;
import com.bluestone.holidayapp.request.NextSameDayHolidayRequestDTO;
import com.bluestone.holidayapp.response.NextSameDayHolidayResponse;
import com.bluestone.holidayapp.response.NextSameDayHolidayResponseDTO;
import com.bluestone.holidayapp.service.HolidayService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class HolidayController
{
    private final Mapper<NextSameDayHolidayRequestDTO, NextSameDayHolidayRequest> nextSameDayHolidayRequestMapper;

    private final Mapper<NextSameDayHolidayResponse, NextSameDayHolidayResponseDTO> nextSameDayHolidayResponseDTOMapper;

    private final HolidayService holidayService;

    @Autowired
    public HolidayController(Mapper<NextSameDayHolidayRequestDTO, NextSameDayHolidayRequest> nextSameDayHolidayRequestMapper,
                             Mapper<NextSameDayHolidayResponse, NextSameDayHolidayResponseDTO> nextSameDayHolidayResponseDTOMapper,
                             HolidayService holidayService)
    {
        this.nextSameDayHolidayRequestMapper = nextSameDayHolidayRequestMapper;
        this.nextSameDayHolidayResponseDTOMapper = nextSameDayHolidayResponseDTOMapper;
        this.holidayService = holidayService;
    }

    @GetMapping
    public NextSameDayHolidayResponseDTO getNextSameDayHoliday(@Valid NextSameDayHolidayRequestDTO requestDTO)
            throws ExecutionException, InterruptedException, NextSameDayHolidayNotFoundException
    {
        NextSameDayHolidayRequest request = nextSameDayHolidayRequestMapper.map(requestDTO);
        NextSameDayHolidayResponse response = holidayService.getNextSameDayHoliday(request);
        return nextSameDayHolidayResponseDTOMapper.map(response);
    }
}
